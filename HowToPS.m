function HowToPS(x,y,fs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example file for running the code from [1]
% To run this code you need to include the following codes by yourself:
% 1) a f0 estimator in 'PS_SpeechEnhancement.m', LINE 79  (or alternatively provide a third input to this function)
% 2) a noise PSD estimator needs to be included in 'PS_GetNoisePSD.m', LINE 52
% 3) a noise PSD estimator needs to be included in 'STFT_SpeechEnhancement.m', LINE 75
%
% INPUT: if no input is given the default audio data is used (from [2]), otherwise:
%       x ... clean speech
%       y ... noisy speech
%       fs ... sampling frequency
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 24.07.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "A Pitch-Synchronous Simultaneous 
%     Detection-Estimation Framework for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pparam. 1383–1393,
%     2012.
% [4] S. Gonzalez and M. Brookes, “PEFAC - a pitch estimation algorithm
%     robust to high levels of noise,” IEEE/ACM Trans. Audio, Speech, and
%     Language Processing, vol. 22, no. 2, pparam. 518–530, 2014.
% [5] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-
%     mean square error short-time spectral amplitude estimator,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 32, no. 6, pparam. 1109–1121,
%     1984.
% [6] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square error log-
%     spectral amplitude estimator,” IEEE Trans. Audio, Speech, and Language
%     Process., vol. 33, no. 2, pparam. 443–445, 1985.
% [7] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     HowToPS.m illustrates how to perform speech enhancement in PS domain or in standard STFT.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('./ExternalFunctions/'); % folder containg noise PSD estimator, f0-estimator...
addpath('./Functions/'); % folder containing all functions written for [1]
if nargin < 1
    [x, fs] = audioread('./audio/ProofOfConcept_Clean.wav');
    [y, ~ ] = audioread('./audio/ProofOfConcept_Noisy.wav');
end   


param.fs = fs;
% run [5]:
param.Method = 'LSA';
x_hat.LSA = STFT_SpeechEnhancement(y,param);

% run [1]:
% Without Detection/Estimation Step:
param.DetectionEstimation = 0;
param.Method = 'SDWienerFilter';
x_hat.PSSDWF = PS_SpeechEnhancement(y,param);

% With Detection/Estimation Step:
param.DetectionEstimation = 1;
x_hat.PSSDDE = PS_SpeechEnhancement(y,param);



% play some audio:
soundsc(x_hat.LSA,fs);
pause(length(x)/fs);
soundsc(x_hat.PSSDWF,fs);
pause(length(x)/fs);
soundsc(x_hat.PSSDDE,fs);

end
