function x_hat = PS_SpeechEnhancement(y,param,f0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs Pitch-Synchronous Speech Enhancement as proposed in [1].
%
% INPUT:
% y ... noisy speech, time domain signal
% param ... struct containing parameters for the algorithm
%                            fs ... sampling frequency
%                           win ... window function handle
%                    hop_factor ... hopsize relative to frame size
%                      AlphaVar ... decision directed smoothing constant
%                         G_min ... flooring of the spectral gain, used in the DE framework
%                         G_max ... maximum value of gain function (1)
%                    LowerLimit ... lower limit applied after processing
%                   framelength ... framelength (s)
%                           hop ... hopsize (s)
%                        Method ... estimator used
%           DetectionEstimation ... binary flag indicating of detection/estimation step is performed           
%     DistributionParameterPath ... path of the parameters of the prior distributions for mean estimation (typically './parameters/')
%                  FilterLength ... smoothing filter length for mean estimation (s)
%                         G_min ... flooring of the gain function
%             ApproxFramelength ... framelength in s which should approximately be achieved when we use multiples of the fundamental period (determines K)
%                    FFT_factor ... fftlength = framelength*FFT_factor
%                      alpha_DD ... smoothing constant for decision directed speech variance (or a priori SNR) estimation
%                  FilterLength ... length of the smoothing filter for mean estimation (determines Q)
%                       MinZeta ... minimum a posteriori SNR
%                       MaxZeta ... maximum a posteriori SNR
%                      Floor_xi ... minimum a priori SNR
% f0 ... optional Signallenghtx2 matrix, 1st column corresponds to timestamps, second column to f0-estimates
%
% OUTPUT:
% x_hat ... enhanced speech signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "A Pitch-Synchronous Simultaneous 
%     Detection-Estimation Framework for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pp. 1383–1393,
%     2012.
% [4] S. Gonzalez and M. Brookes, “PEFAC - a pitch estimation algorithm
%     robust to high levels of noise,” IEEE/ACM Trans. Audio, Speech, and
%     Language Processing, vol. 22, no. 2, pp. 518–530, 2014.
% [5] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-
%     mean square error short-time spectral amplitude estimator,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 32, no. 6, pp. 1109–1121,
%     1984.
% [6] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square error log-
%     spectral amplitude estimator,” IEEE Trans. Audio, Speech, and Language
%     Process., vol. 33, no. 2, pp. 443–445, 1985.
% [7] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_SpeechEnhancement.m performs pitch-synchronous speech enhancement.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (1) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1
    param = PS_SetParameters(); % simply set default parameters
else
    param = PS_SetParameters(param);  % only set those parameters which are not defined
end
fs = param.fs;
SignalLength = length(y);

%%%%%%%%%%%%%%%%%%% (2) f0 -estimation [4]: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) PUT YOUR FUNDAMENTAL FREQUENCY ESTIMATOR HERE
% 2) for [1] we used [4] with hopsize 0.004                                      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [f0,t_f0,pv]= fxpefac(y,fs,0.004); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    t_f0 = f0(:,1);
    f0 = f0(:,2);
end

%%%%%%%%%%%%%%%%%%% (3) Pitch-synchronous sliding window DFT: %%%%%%%%%%%%%
[Y,WindowImpact,t_frames,idx_start,idx_end,fftlength,NDFT_max,t,f0_sig,HarmonicToDFT,DATA] = PS_swDFT(y,f0,t_f0,param);

%%%%%%%%%%%%%%%%%%% (4) Phase Normalization: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Y_PhaseNormalized, PhaseProgression] = PS_PhaseNormalization(Y,WindowImpact,f0_sig,fs,NDFT_max,t,t_frames,HarmonicToDFT);

%%%%%%%%%%%%%%%%%%% (5) Noise PSD Estimation [3]: %%%%%%%%%%%%%%%%%%%%%%%%%
D_p = PS_GetNoisePSD(y,Y,t_frames,fftlength,param); % Estimate Noise PSD and interpolate it to PS time-frequency points
D_p(D_p<0) = 0;

%%%%%%%%%%%%%%%%%%% (6) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation
G = zeros(size(Y));
Y_p = Y.*conj(Y);
zeta = max(min(Y_p./D_p,param.MaxZeta),param.MinZeta);
zeta(abs(Y)==0) = 0; 
zeta(zeta==param.MaxZeta) = param.MaxZeta; 

% initialize container struct
DATA.xi = zeros(size(Y)); % a priori SNR
DATA.xi_unsmoothed_l = 1;
DATA.FrameIndices = t_frames;
DATA.Y_PhaseNormalized = Y_PhaseNormalized;

% A simple decision directed loop [5]:
NrOfFrames = size(Y,2);
for FrameIndex = 1:NrOfFrames    
    Y_l = Y_PhaseNormalized(:,FrameIndex); 
    zeta_l = zeta(:,FrameIndex);
    NoisePSD_l = D_p(:,FrameIndex);
    [G_l, DATA] = PS_GetGain(NoisePSD_l,zeta_l,Y_l,DATA,param,FrameIndex);
    G(1:floor(fftlength(FrameIndex)/2+1),FrameIndex)  = min(G_l(1:floor(fftlength(FrameIndex)/2+1)),param.G_max); 
end

G = max(abs(G),param.LowerLimit).*exp(1i*angle(G));
X_hat = G.*Y_PhaseNormalized; % apply complex gain function on the noisy signal
X_hat = X_hat.*exp(1i*PhaseProgression)./WindowImpact; % undo phase normalization and linear window phase subtraction
X_hat(isnan(X_hat)) = 0; % catch any NaN values (sometimes there are some)

%%%%%%%%%%%%%%%%%%% (7) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
x_hat = PS_OLA(X_hat,SignalLength,idx_start,idx_end,fftlength,param);

end




