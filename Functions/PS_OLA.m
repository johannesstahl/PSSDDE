function x_hat = PS_OLA(X,SignalLength,idx_start,idx_end,fftlength,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PS overlap-add as described in [1].
%
% INPUT:
% X ... PSswDFT of a signal
% SignalLength ... length of the reconstructed signal
% idx_start ... start indices for all frames
% idx_end ... end indices for all frames
% fftlength ... fft lengths for all frames
% param ... struct containing parameters:
%                            FFT_factor ... fftlength = framelength*FFT_factor
%                                   win ... window function handle
%
% OUTPUT:
% x_hat ... synthesized signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_OLA.m computes PS overlap-add.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Revision: Update: incorporated FFT_factor.

FFT_factor = param.FFT_factor;
NrOfFrames = size(X,2);
ReconstructionMatrix = zeros(NrOfFrames,SignalLength);
ReconstructionMatrixSynWin = zeros(NrOfFrames,SignalLength);

for FrameIndex = 1:NrOfFrames  
    window = param.win(fftlength(FrameIndex)/FFT_factor);
    synwin = window;
    XReconst_temp = X(1:floor(fftlength(FrameIndex)/2+1),FrameIndex);
    XReconst_temp = [XReconst_temp; conj(flipud(XReconst_temp(2:end)))];
    TDsig = real(ifft(XReconst_temp,fftlength(FrameIndex)));
    xReconst_temp = TDsig(1:(fftlength(FrameIndex)/FFT_factor))*sum(window);
    xReconst_temp = (synwin).*xReconst_temp./window;
    ReconstructionMatrixSynWin(FrameIndex,idx_start(FrameIndex):idx_end(FrameIndex)) = synwin;
    ReconstructionMatrix(FrameIndex,idx_start(FrameIndex):idx_end(FrameIndex)) = xReconst_temp(1:(fftlength(FrameIndex)/FFT_factor));  
end
% normalization with sum of windows
Normalizer = sum(ReconstructionMatrixSynWin).';
x_hat = sum(ReconstructionMatrix).'./Normalizer;
x_hat(isnan(x_hat)) = 0;
