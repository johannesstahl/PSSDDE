function x_hat = STFT_OLA(x_framed,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform overlap-add.
%
% INPUT:
% x_framed ... signal in blocks of length N (therfere dimensions need to be Nx#frames) 
% param ... struct containing parameters for the algorithm
%                            fs ... sampling frequency
%                           win ... window function handle
%                           hop ... hopsize (s)
% OUTPUT:
% x_hat ... enhanced speech signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     STFT_OLA.m performs standard overlap-add.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Synthesis Window:
NrOfFrames = size(x_framed,2);
N = size(x_framed,1);
window = param.win(N);
hop = param.hop*param.fs; 
window= window/(sqrt(sum(window(1:hop:N).^2))); % normalization of the window (ensure COLA)

x_framed = x_framed.*window(:,ones(1,NrOfFrames));
x_hat = zeros(hop*(NrOfFrames-1) + N,NrOfFrames);
for kk = 1:NrOfFrames
    x_hat(((kk-1)*hop+1):((kk-1)*hop + N),kk) = x_framed(:,kk);
end
x_hat = sum(x_hat,2); 
end