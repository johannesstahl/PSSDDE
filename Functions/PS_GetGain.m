function [G_l, DATA] = PS_GetGain(NoisePSD_l,zeta_l,Y_l,DATA,param,FrameIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes Gain for various estimators.
%
% INPUT:
% NoisePSD_l ... Noise PSD at frame l
% zeta_l ... a posteriori SNR at frame l
% Y_l ... noisy observation at frame l
% DATA ... struct containing data matrices (see below, OUTPUT for details)
% param ... struct containing parameters:
%                            fs ... sampling frequency
%                         G_min ... flooring of the spectral gain, used in the DE framework
%                        Method ... estimator used
%           DetectionEstimation ... binary flag indicating of detection/estimation step is performed           
%     DistributionParameterPath ... path of the parameters of the prior distributions for mean estimation (typically './parameters/')
%                      alpha_DD ... smoothing constant for decision directed speech variance (or a priori SNR) estimation
%                  FilterLength ... length of the smoothing filter for mean estimation (s) (determines Q)
%                      Floor_xi ... minimum a priori SNR
%
% OUTPUT:
% G_l ... gain for frame l
% DATA ... struct containing data that is needed in the next iterations (e.g. speech variance estimates):
%           xi_unsmoothed_l ... a priori SNR from previous frame
%              FrameIndices ... absolute indices of frames (in order to evaluate how many frames are within a 20 ms timespan)
%                    Y_mean ... matrix containing mean estimate
%         Y_PhaseNormalized ... matrix containg original noisy PSswDFT
%         VarS_unsmoothed_l ... speech variance estimate from previous frame
%   HarmonicMainlobeIndices ... Indices associated to window mainlobe around a harmonic
%                        xi ... a priori SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 20.07.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "A Pitch-Synchronous Simultaneous 
%     Detection-Estimation Framework for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_GetGain.m computes spectro-temporal gains.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% for non SD methods:
xi_l = max(param.alpha_DD*DATA.xi_unsmoothed_l+(1-param.alpha_DD)*max(zeta_l-1,param.Floor_xi),param.Floor_xi); % Update a priori SNR

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%% Estimate Parameters of the Distribution %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (strcmp(param.Method,'SDWienerFilter') || strcmp(param.Method,'SpectralMean') || strcmp(param.Method,'SDWienerFilterOld') )
    % Smoothing Parameters:
    if FrameIndex > 1
        [~,StartIndex] = min(abs(DATA.FrameIndices(FrameIndex)-DATA.FrameIndices -param.FilterLength));
        DATA.Y_mean(:,FrameIndex) = nanmean(DATA.Y_PhaseNormalized(:,StartIndex:FrameIndex),2);
        Q = length(StartIndex:FrameIndex); % filter length
        % DD Variance Estimation Eq. (47) in [1]
        VarS_l = max(param.alpha_DD*DATA.VarS_unsmoothed_l+...
            (1-param.alpha_DD)*max(abs(Y_l-DATA.Y_mean(:,FrameIndex)).^2- NoisePSD_l,0),param.VarS_min); % 
    else
        % Initialization:
        StartIndex = 1;
        Q = 1;
        DATA.Y_mean(:,FrameIndex) = zeros(size(Y_l));
        VarS_l = zeros(size(Y_l));
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Mean Estimation With Fixed Prior %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   %%%%%%%%%%%%%%%%% load prior parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if param.FilterLength ~= 0
        load([param.DistributionParameterPath 'DistributionParameters_FIR' num2str(param.fs) '_Revision_FL' strrep(num2str(param.FilterLength),'.','_')])
    else
    	load([param.DistributionParameterPath 'DistributionParameters_FIR' num2str(param.fs) '_Revision_FL' strrep(num2str(0.02),'.','_')])
        Q = 1;
    end

   % we only need hypothesis 1 since we set all bins for hypothesis 2 to
   % zero
   mu = DistributionParameters.H1(:,2);
   nu = DistributionParameters.H1(:,1);
   if size(mu,1) < length(Y_l)
    	mu = [mu; mu(end)*ones(length(Y_l)-size(nu,1),1)];
        nu = [nu; nu(end)*ones(length(Y_l)-size(nu,1),1)];    
   end
   R = abs(DATA.Y_mean(:,FrameIndex));
   % Normalization:
   if FrameIndex == 1
    NormConst = 1;
   else
    NormStuff = abs(fft(irfft(DATA.S_hat_l)/max(abs(irfft(DATA.S_hat_l)))));  
    NormConst = mean(abs(NormStuff(2:(sum(NoisePSD_l>0)-1))));
   end
   
   sigma = NoisePSD_l + VarS_l; % overall variance
   
   alpha_MAP = angle(DATA.Y_mean(:,FrameIndex));
   Rq = abs(DATA.Y_PhaseNormalized(:,StartIndex:FrameIndex));
   
   % Normalization:
   varthetaq = angle(DATA.Y_PhaseNormalized(:,StartIndex:FrameIndex));
   u = sum(Rq.*cos(varthetaq-alpha_MAP(:,ones(1,Q))),2)/(2*Q) - sigma.*mu./(4*Q*NormConst); % *NormConst
   z_l = u + sqrt(u.^2 + sigma.*(nu-1)/(2*Q));
   z_l = real(z_l);
   z_l(R == 0) = 0;
   z_l = abs(z_l).*exp(1i*alpha_MAP);
   
   HarmonicMainlobeIndices = DATA.HarmonicMainlobeIndices;
   Indices = logical(zeros(size(z_l)));
   Indices(HarmonicMainlobeIndices(:)) = true;
   
   % Model Assumptions:
   z_l(HarmonicMainlobeIndices(:,1),:) = 0.5*z_l(HarmonicMainlobeIndices(:,2),:);
   z_l(HarmonicMainlobeIndices(:,3),:) = 0.5*z_l(HarmonicMainlobeIndices(:,2),:);     
   z_l(~Indices) = 0;
   DATA.Y_mean(:,FrameIndex) = z_l; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% Evaluate Gain %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch param.Method
    case '0'
        G_l = ones(size(xi_l));
    case 'WienerFilter'
        G_l = xi_l./(1+xi_l);
    case 'SpectralMean' % only use spectral mean estimate dwith fixed priors
        S_hat = z_l;
    	G_l = S_hat./Y_l;
        G_l(isnan(abs(G_l)) | isinf(G_l)) = 0;   
    case 'SDWienerFilter' % [1]
        GSW = VarS_l./(VarS_l + NoisePSD_l);
        GDW = 1-GSW;
        S_hat = GDW.*z_l + GSW.*Y_l;
        G_l = S_hat./Y_l;
        G_l(isnan(abs(G_l)) | isinf(G_l)) = param.G_min;%GSW(isnan(abs(G_l)) | isinf(G_l));
        
        if param.DetectionEstimation % [1]
        	G2 = G_l;
            G1 = GSW;
            G0 = param.G_min*ones(size(G_l));
            lambda = [3 2 0.1; 1 2 1; 0.1 2 3];
            
            % priors:
            pH2 = zeros(size(Y_l));
            pH2(HarmonicMainlobeIndices(:)) = 1/6;
            pH0 = (1-pH2)/2;
            pH1 = (1-pH2)/2;
                    
              
            % likelihoods:
            epsilon = 1e-10; % to ensure numerical stability
            Gauss = @(data,var,mean,epsilon) exp(-(abs(data-mean).^2)./(var+epsilon))./(pi*(var+epsilon));       
            pYH0 = Gauss(Y_l,NoisePSD_l,0,epsilon);
           
            % Revision: do not add abs(z_l)^2
            pYH1 = Gauss(Y_l,NoisePSD_l +VarS_l,0,epsilon);
            pYH2 = Gauss(Y_l,NoisePSD_l+VarS_l,z_l,epsilon);
            
            % likelihoodratios:
            LikelihoodRatio = @(pHb,pYHb,pH0,pYH0,epsilon) pHb.*pYHb./(pH0.*pYH0+epsilon);       
            LAMBDA1 = LikelihoodRatio(pH1,pYH1,pH0,pYH0,epsilon);
            LAMBDA2 = LikelihoodRatio(pH2,pYH2,pH0,pYH0,epsilon);
            
            G_b0 = (lambda(1,1).*G0 + LAMBDA1.*lambda(1,2).*G1+ LAMBDA2.*lambda(1,3).*G2)...
                   ./(lambda(1,1) + LAMBDA1.*lambda(1,2)+ LAMBDA2.*lambda(1,3));
            G_b1 = (lambda(2,1).*G0 + LAMBDA1.*lambda(2,2).*G1+ LAMBDA2.*lambda(2,3).*G2)...
                   ./(lambda(2,1) + LAMBDA1.*lambda(2,2)+ LAMBDA2.*lambda(2,3));
            G_b2 = (lambda(3,1).*G0 + LAMBDA1.*lambda(3,2).*G1+ LAMBDA2.*lambda(3,3).*G2)...
                   ./(lambda(3,1) + LAMBDA1.*lambda(3,2)+ LAMBDA2.*lambda(3,3));
               
            G_b = [G_b0 G_b1 G_b2];
             
            % risk estimation      
            rb0H0 = lambda(1,1)*pYH0.*abs(param.G_min-G_b0).^2.*abs(Y_l).^2;
            rb1H0 = lambda(2,1)*pYH0.*abs(param.G_min-G_b1).^2.*abs(Y_l).^2;
            rb2H0 = lambda(3,1)*pYH0.*abs(param.G_min-G_b2).^2.*abs(Y_l).^2;
                   
            rb0H1 = lambda(1,2)*pYH1.*(NoisePSD_l.*GSW +abs(Y_l.*GSW-Y_l.*G_b0).^2);
            rb1H1 = lambda(2,2)*pYH1.*(NoisePSD_l.*GSW +abs(Y_l.*GSW-Y_l.*G_b1).^2);
            rb2H1 = lambda(3,2)*pYH1.*(NoisePSD_l.*GSW +abs(Y_l.*GSW-Y_l.*G_b2).^2);
            
            rb0H2 = lambda(1,3)*pYH2.*(NoisePSD_l.*GSW + abs(Y_l.*GSW+z_l.*GDW - G_b0.*Y_l).^2);
            rb1H2 = lambda(2,3)*pYH2.*(NoisePSD_l.*GSW + abs(Y_l.*GSW+z_l.*GDW - G_b1.*Y_l).^2);
            rb2H2 = lambda(3,3)*pYH2.*(NoisePSD_l.*GSW + abs(Y_l.*GSW+z_l.*GDW - G_b2.*Y_l).^2);
                   
            rb0 =  pH0.*rb0H0 + pH1.*rb0H1 + pH2.*rb0H2;
            rb1 =  pH0.*rb1H0 + pH1.*rb1H1 + pH2.*rb1H2;
            rb2 =  pH0.*rb2H0 + pH1.*rb2H1 + pH2.*rb2H2;
            Risk = [rb0 rb1 rb2]; 
                  
            [val, ind] = min(Risk.'); % obtain minima of risk
            SelectedEstimate = zeros(length(ind),3);
            SelectedEstimate(ind==1,1) = ind(ind== 1);
            SelectedEstimate(ind==2,2) = ind(ind == 2);
            SelectedEstimate(ind==3,3) = ind(ind == 3);
            SelectedEstimate(SelectedEstimate>0) = 1;
            DATA.SelectedEstimate(FrameIndex,:) = ind;
            G_l = sum(G_b.*SelectedEstimate,2); % select estimate wiht lowest risk  
            z_l(ind~=2) = 0;
        end
        G_l(isnan(abs(G_l)) | isinf(G_l)) = 0;
        S_hat = Y_l.*G_l;
        DATA.S_hat_l = S_hat;
end

if (strcmp(param.Method,'SDWienerFilter') || strcmp(param.Method,'SpectralMean') || strcmp(param.Method,'SDWienerFilterOld') )
    DATA.VarS_unsmoothed_l = max(abs(S_hat-z_l).^2,0);%
end
  
DATA.xi(:,FrameIndex) = xi_l;
DATA.xi_unsmoothed_l = zeta_l.*abs(G_l).^2; 
end

