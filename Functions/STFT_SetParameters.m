function param = STFT_SetParameters(param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializes parameters as in [1]
%
% Input:
% y ... noisy speech, time domain signal
% param ... struct containing parameters for the algorithm
%                            fs ... sampling frequency
%                   framelength ... framelength (s)
%                           hop ... hop size (s)
%                           win ... window function handle
%                        Method ... chosen estimator (e.g. 'LSA')
%                      alpha_DD ... smoothing factor for decision directed approach
%                    FFT_factor ... factor determining fftlength = N*FFT_factor
%                         G_max ... maximum value of gain function
%                         G_min ... floor for spectro-temporal gain function
%     DistributionParameterPath ... if distributions are chosen frequency dependent the correct parameters are found on this path (e.g. './parameters')
%                       MinZeta ... minimum a posteriori SNR
%                       MaxZeta ... maximum a posteriori SNR
%                      Floor_xi ... minimum for a priori SNR
% Output:
% x_hat ... enhanced speech signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     STFT_SetParameters.m initializes parameters for STFT speech enhancement.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p.fs = 16e3;
p.framelength =  32e-3;
p.FFT_factor = 1;
p.win = @(N) sqrt(hamming(N,'periodic'));
p.hop = 4e-3; % hopsize relative to framelength
p.Method = 'WienerFilter'; % Speech Spectral Estimator
p.alpha_DD = 0.98; % smoothing factor for DD variance estimation
p.DistributionParameterPath = './parameters/'; 
G_min = -20; % dB
p.G_min = 10^(G_min/20);
p.G_max = 1;
p.MinZeta = 0.001;    % min posterior SNR as a power ratio [0.001 = -30dB]
p.MaxZeta=1000;     % maximum posterior SNR = 30dB
p.Floor_xi = 0; % floor of the a priori SNR       
if nargin == 0
    param = p;
else
    fields = fieldnames(p);
    for kk = 1:length(fields)
        if ~isfield(param,fields{kk})
            val = getfield(p,fields{kk});
            param = setfield(param,fields{kk},val);
        end
    end
end