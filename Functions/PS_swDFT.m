function [X,WindowImpact,t_frames,idx_start,idx_end,fftlength,NDFT_max,t, f0_sig,HarmonicToDFT,DATA] = PS_swDFT(x,f0,t_f0,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pitch-synchronously frames the signal according to f0-estimates and
% computes the sliding window DFT like in [1].
% 
% INPUT: 
%       x ... signal
%       f0 ... fundamental frequency estimates 
%       t_f0 ... time stamps of the respective f0 estimates
%       param:
%                       fs ... sampling frequency
%                      win ... window handle
%               FFT_factor ... factor which defines  FFT length = FFT_factor*framelength
%        ApproxFramelength ... frame length which is approximately expected (e.g. stationarity interval of the analyzed signal)
%               hop_factor ... hop = framelength*hop_factor
%                   f0_min ... minimum frequency considered (determines maximum framelength)
%                   f0_max ... maximum frequency considered (determines minimum framelength)
%
% OUTPUT:
%       X ... matrix containing the PS sliding window DFT
%       WindowImpact ... matrix containing linear phase of the causal window
%       t_frames ... timestamps for allframes
%       idx_start ... start indices for all frames
%       idx_end ... end indices for all frames
%       fftlength ... fft lengths for all frames
%       NDFT_max ... maximum DFT length (is determined by maximal framelength)
%       t ... timestamp of each sample (increment: 1/fs)
%       f0_sig ... f0 values interpolated to each sample @ fs
%       HarmonicToDFT ... assigns harmonic parameters to DFT bins (matrix)
%       DATA ... container for data:
%                   HarmonicMainlobeIndices ... Indices associated to window mainlobe around a harmonic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% This implemenation is not very efficient...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_swDFT.m pitch-synchronously frames the signal according to f0-estimates and computes the sliding window DFT (PSswDFT).
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (1) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 4
    param = PS_SetParameters();
end
%%%%%%%%%%%%%%%%%%%%% ANALYSE SPEECH PITCH-SYNCHRONOUSLY %%%%%%%%%%%%%%%%%%
fs = param.fs;
SignalLength = length(x);
t = (0:(SignalLength -1)).'/fs;
FFT_factor = param.FFT_factor;

ApproxFramelength = param.ApproxFramelength*fs; % (samples), approximate framelength that should be used 
ApproxFramelength = ApproxFramelength + mod(ApproxFramelength+1,2); % ensure that we have an odd number of analysis samples

% Choose integer number of fundamental periods per frame dependent on some
% assumptions on the fundamental frequency
T0_max = ceil(fs/param.f0_min); % samples

f0_sig = interp1(t_f0,f0,t,'linear','extrap'); % map to all available time instances
f0_sig(f0_sig<param.f0_min) = param.f0_min; % ensure that the range of f0 fits the frame-setup determining boundaries
f0_sig(f0_sig>param.f0_max) = param.f0_max; 

% Get optimal number of periods per frame considering the given setup
NrOfPeriodsPerFrame = round(ApproxFramelength/(2*fs/(param.f0_max+param.f0_min)))-1; 
NDFT_max = FFT_factor*(2^(nextpow2(T0_max*NrOfPeriodsPerFrame)));

HarmonicIndices = (1+NrOfPeriodsPerFrame*FFT_factor):NrOfPeriodsPerFrame*FFT_factor:(NDFT_max/2+1); 
DATA.HarmonicMainlobeIndices = [HarmonicIndices.'-1 HarmonicIndices.' HarmonicIndices.'+1];

HarmonicToDFT = zeros(NDFT_max/2+1,1); % assigns dominant harmonics to corresponding DFT bins  
HarmonicToDFT(round((1+NrOfPeriodsPerFrame*FFT_factor)/2):(NDFT_max/2+1))=interp1(HarmonicIndices,1:length(HarmonicIndices), ...
                                        round((1+NrOfPeriodsPerFrame*FFT_factor)/2):(NDFT_max/2+1),'nearest','extrap');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% Obtain the pitch-synchronous representations %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization:
% maximum framelength is given as the NrOfPeriodsPerFrame times the maximum
% fundamental period:
frame_durations = round((NrOfPeriodsPerFrame*fs./f0_sig.'+1)/2)*2-1;
NrOfFramesMax = length(frame_durations);
X = zeros(floor(NDFT_max/2+1),NrOfFramesMax);
WindowImpact = zeros(floor(NDFT_max/2+1),NrOfFramesMax);
idx_start = zeros(1,NrOfFramesMax);
idx_end = zeros(1,NrOfFramesMax);
hop_PS = zeros(1,NrOfFramesMax);
fftlength = zeros(1,NrOfFramesMax);
frame_indices = zeros(1,NrOfFramesMax);
frame_indices(1) = sum(0>((1:length(frame_durations))-(frame_durations-1)/2))+2;
FrameIndex = 0;
% Compute PS-sliding window DFT:
while max(idx_end)<length(x)
    FrameIndex = FrameIndex + 1;
    if frame_indices(FrameIndex) > numel(frame_durations)
                frame_indices = frame_indices(1:(FrameIndex-1));
        break
    end
   
    hop_PS(FrameIndex) = round((frame_durations(frame_indices(FrameIndex)))*param.hop_factor);
    if max(idx_end)<length(x)
    	frame_indices(FrameIndex+1) = frame_indices(FrameIndex)+hop_PS(FrameIndex);
    end
    idx_start(FrameIndex) = max(frame_indices(FrameIndex) - (frame_durations(frame_indices(FrameIndex))-1)/2,1);
    idx_end(FrameIndex) = frame_indices(FrameIndex) + (frame_durations(FrameIndex)-1)/2; idx_end(FrameIndex) = idx_end(FrameIndex) + mod(idx_end(FrameIndex)-idx_start(FrameIndex),2); idx_end(FrameIndex) = min(idx_end(FrameIndex),SignalLength);

    fftlength(FrameIndex) = min(FFT_factor*(idx_end(FrameIndex)-idx_start(FrameIndex) + 1),NDFT_max);
    
    frame_duration = frame_durations(frame_indices(FrameIndex));
    idx_start(FrameIndex) = max(frame_indices(FrameIndex) - (frame_duration-1)/2,1);
    idx_end(FrameIndex) = frame_indices(FrameIndex) + (frame_duration-1)/2; 
    idx_end(FrameIndex) = idx_end(FrameIndex) + mod(idx_end(FrameIndex)-idx_start(FrameIndex),2); 
    idx_end(FrameIndex) = min(idx_end(FrameIndex),SignalLength);
    fftlength(FrameIndex) = min(FFT_factor*(idx_end(FrameIndex)-idx_start(FrameIndex) + 1),NDFT_max); % save fftlength for synthesis
    
    % get rid of linear phase of the window
    if mod(fftlength(FrameIndex),2) % for odd fft lengths
        theta = 2*pi*(0:(fftlength(FrameIndex)/2-1)).'/fftlength(FrameIndex);
        %Only half sided dft is needed:
        WindowImpact(1:floor(fftlength(FrameIndex)/2+1),FrameIndex) = exp(1i*[theta; -pi/2]*(idx_end(FrameIndex)-idx_start(FrameIndex)-1)/2); 
    else % in case of even fft lengths
        theta = 2*pi*(0:(fftlength(FrameIndex)/2-1)).'/fftlength(FrameIndex);
        %Only half sided dft is needed:
        WindowImpact(1:floor(fftlength(FrameIndex)/2+1),FrameIndex) = exp(1i*[theta; -theta(end)]*(idx_end(FrameIndex)-idx_start(FrameIndex)-1)/2);
    end
    
    if ~(idx_end(FrameIndex)-idx_start(FrameIndex) + 1 < 0)
        window = param.win(length(idx_start(FrameIndex):idx_end(FrameIndex)));%win(idx_end(FrameIndex)-idx_start(FrameIndex) + 1);
        % FFT of the noisy signal
        FullDFT = fft(x(idx_start(FrameIndex):idx_end(FrameIndex)).*window,fftlength(FrameIndex))/sum(window);
        X(1:floor(fftlength(FrameIndex)/2+1),FrameIndex) = FullDFT(1:floor(fftlength(FrameIndex)/2+1));
    end
end
% Throw away redundant frames:
NrOfFrames = FrameIndex; 
X = X(:,1:NrOfFrames);
WindowImpact = WindowImpact(:,1:NrOfFrames);
idx_start = idx_start(1:NrOfFrames);
idx_end = idx_end(1:NrOfFrames);
fftlength = fftlength(1:NrOfFrames);
frame_indices = frame_indices(1:NrOfFrames);
frame_indices(1) = sum(0>((1:length(frame_durations))-(frame_durations-1)/2))+2;
t_frames = frame_indices/fs;
DATA.HarmonicMainlobeIndices(DATA.HarmonicMainlobeIndices(:,3)>(NDFT_max/2+1),:) = [];

