function [Y_PhaseNormalized, PhaseProgression] = PS_PhaseNormalization(Y,WindowImpact,f0_sig,fs,NDFT_max,t,t_frames,HarmonicToDFT)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute Phase normalization as explained in [1].
% 
% INPUT: 
%       Y ... swDFT
%       WindowImpact ... matrix containing linear phase of the causal window
%       f0_sig ... f0 values interpolated to each sample @ fs
%       fs ... sampling frequency
%       NDFT_max ... maximum DFT length (is determined by maximal framelength)
%       t ... timestamp of each sample (increment: 1/fs)
%       t_frames ... timestamps for allframes
%       HarmonicToDFT ... assigns harmonic parameters to DFT bins (matrix)
%
% OUTPUT:
%       Y_PhaseNormalized ... matrix containing the phase normalized PSswDFT
%       PhaseProgression ... matrix containing the linear phase progression along time due to the sliding window
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_PhaseNormalization.m computes phase normalization.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Phase evolves linearly along time for harmonics:
PhaseProgression = filter(1,[1 -1],f0_sig*2*pi/fs);
% Assign the corresponding phase values to harmonics assuming strictly
% harmonically realated frequencies
PhaseProgression = HarmonicToDFT(1:floor(NDFT_max/2+1))*interp1(t,PhaseProgression.',t_frames,'linear','extrap');
% "phase normalization" means getting rid of linear window phase and linear phase progression along timw:
Y_PhaseNormalized = Y.*exp(-1i*PhaseProgression).*WindowImpact;
