function [x_framed, t_frames] = FramingAndWindowing(x,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% splits signal into (overlapping) frames and multiplies them with the selected window
% function (fixed sliding window).
% 
% INPUT:
% x ... input signal
% param ... contains parameters:
%           win ... handle for window function
%           framelength ... frame lenght in seconds
%           fs ... sampling frequency
%           hop ... hopsize in seconds
%
% OUTPUT:
% x_framed ... framed and windowed signal
% t_frames ... timestamps of individual frames in seconds 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     FramingAndWindowing.m splits signal into (overlapping) frames and multiplies them with the selected window function (fixed sliding window).
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = round(param.framelength*param.fs);
hop = round(param.hop*param.fs);
window = param.win(N);
window= window/(sqrt(sum(window(1:hop:N).^2))); % normalization
x_framed = buffer(x,N,N-hop,'nodelay'); % framing the signal
NrOfFrames = size(x_framed,2); 
x_framed = x_framed.*window(:,ones(1,NrOfFrames)); % windowing the signal
t_frames = ((N/2+1):hop:(length(x)-N/2 + 1))/param.fs; % frame-center time-stamps
end