function param = PS_SetParameters(param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializes parameters as in [1]
%
% INPUT:
% param ... struct containing parameters for the algorithm
%                            fs ... sampling frequency
%                           win ... window function handle
%                    hop_factor ... hopsize relative to frame size
%                         G_min ... flooring of the spectral gain, used in the DE framework
%                         G_max ... maximum value of gain function (1)
%                    LowerLimit ... lower limit applied after processing
%                   framelength ... framelength (s)
%                           hop ... hopsize (s)
%                        Method ... estimator used
%           DetectionEstimation ... binary flag indicating of detection/estimation step is performed           
%     DistributionParameterPath ... path of the parameters of the prior distributions for mean estimation (typically './parameters/')
%             ApproxFramelength ... framelength in s which should approximately be achieved when we use multiples of the fundamental period (determines K)
%                    FFT_factor ... fftlength = framelength*FFT_factor
%                      alpha_DD ... smoothing constant for decision directed speech variance (or a priori SNR) estimation
%                  FilterLength ... length of the smoothing filter for mean estimation (s) (determines Q)
%                       MinZeta ... minimum a posteriori SNR
%                       MaxZeta ... maximum a posteriori SNR
%                      Floor_xi ... minimum a priori SNR
%                        f0_min ... minimum frequency considered (determines maximum framelength)
%                        f0_max ... maximum frequency considered (determines minimum framelength)
%
% OUTPUT:
% param ... complete struct (if some parameters have not been set a priori)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_SetParameters.m sets the parameters for the PS processing.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.ApproxFramelength =  32e-3;
p.win = @(N) (hamming(N,'periodic'));
p.hop_factor =1/8; % hopsize relative to framelength
p.FFT_factor = 1;       
p.DetectionEstimation = 1;
p.Method = 'SDWienerFilter';%'SDWienerFilter'; % Speech Spectral Estimator
p.alpha_DD = 0.98; % smoothing factor for DD variance estimation
p.DistributionParameterPath = './parameters/';
p.FilterLength = 0.02; % moving average filter lenght [s] for mean estimation  
G_min = -20; % dB
LowerLimit = -Inf; % dB
p.G_min = 10^(G_min/20);
p.LowerLimit = 10^(LowerLimit/20); % lower limit after applying the processing chain
p.G_max = 1;
p.MinZeta = 0.001;    % min posterior SNR as a power ratio [0.001 = -30dB]
p.MaxZeta=1000;     % maximum posterior SNR = 30dB
p.Floor_xi = 0; % floor of the a priori SNR       
p.fs = 16e3;
p.f0_max = 350; % Hz
p.f0_min = 90; %
p.VarS_min = 0;
if nargin == 0
    param = p;
else
    fields = fieldnames(p);
    for kk = 1:length(fields)
        if ~isfield(param,fields{kk})
            val = getfield(p,fields{kk});
            param = setfield(param,fields{kk},val);
        end
    end
end