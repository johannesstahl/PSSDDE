function x_hat = STFT_SpeechEnhancement(y,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs Speech Enhancement in STFT domain based on [5].
%
% INPUT:
% y ... noisy speech
% param ... struct containing parameters for the algorithm, if not set, default values are chosen
%                            fs ... sampling frequency
%                   framelength ... framelength (s)
%                           hop ... hop size (s)
%                           win ... window function handle
%                        Method ... chosen estimator (e.g. 'LSA')
%                      alpha_DD ... smoothing factor for decision directed approach
%                    FFT_factor ... factor determining fftlength = N*FFT_factor
%                         G_max ... maximum value of gain function
%                         G_min ... floor for spectro-temporal gain function
%     DistributionParameterPath ... if distributions are chosen frequency dependent the correct parameters are found on this path (e.g. './parameters')
%                       MinZeta ... minimum a posteriori SNR
%                       MaxZeta ... maximum a posteriori SNR
%                      Floor_xi ... minimum for a priori SNR
%
% OUTPUT:
% x_hat ... enhanced speech
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
% [2] J. S. Garofolo, L. F. Lamel, W. M. Fisher, J. G. Fiscus, D. S.
%     Pallett, and N. L. Dahlgren, “DARPA TIMIT acoustic phonetic
%     continuous speech corpus CDROM,” 1993. [Online]. Available:
%     http://www.ldc.upenn.edu/Catalog/LDC93S1.html
% [3] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pp. 1383–1393,
%     2012.
% [4] S. Gonzalez and M. Brookes, “PEFAC - a pitch estimation algorithm
%     robust to high levels of noise,” IEEE/ACM Trans. Audio, Speech, and
%     Language Processing, vol. 22, no. 2, pp. 518–530, 2014.
% [5] Y. Ephraim and D. Malah, “Speech enhancement using a minimum-
%     mean square error short-time spectral amplitude estimator,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 32, no. 6, pp. 1109–1121,
%     1984.
% [6] Y. Ephraim and D. Malah, “Speech enhancement using a minimum mean-square error log-
%     spectral amplitude estimator,” IEEE Trans. Audio, Speech, and Language
%     Process., vol. 33, no. 2, pp. 443–445, 1985.
% [7] VOICEBOX: MATLAB toolbox for speech processing.
%     Home page: http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/voicebox.html
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     STFT_SpeechEnhancement.m performs STFT speech enhancement.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (1) Read-in parameters: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1
    param = STFT_SetParameters(); % simply set default parameters
else
    param = STFT_SetParameters(param);  % only set those parameters which are not defined
end

%%%%%%%%%%%%%%%%%%% (2) STFT: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = param.framelength*param.fs; % framelength in samples
[y_framed, t_frames] = FramingAndWindowing(y,param);
fftlength = N*param.FFT_factor;
Y = fft(y_framed,fftlength); % half sided stft
Y = Y(1:(fftlength/2+1),:);

%%%%%%%%%%%%%%%%%%% (3) Noise PSD Estimation [3]: %%%%%%%%%%%%%%%%%%%%%%%%%
hop_noise = param.hop;
Y_p = Y.*conj(Y); % power spectrum of half sided stft
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) PUT YOUR NOISE PSD ESTIMATOR HERE
% 2) for [1] we used [3]      
% 3) should have the variable name D_p and dimensions: (fftlength/2+1) x #frames                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D_p=estnoiseg(Y_p.',hop_noise).';   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% (6) Speech Estimation: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init estimation:
G = zeros(size(Y));
xi = zeros(size(Y));
xi_unsmoothed_l = 1;
zeta = max(min(Y_p./D_p,param.MaxZeta),param.MinZeta);

% A simple decision directed loop [5]:
hop_ratio = hop_noise/param.hop;% ratio of hopsize of noise psd estimator to hopsize of the speech estimator
NrOfFrames = size(y_framed,2);
for FrameIndex = 1:NrOfFrames
    zeta_l = zeta(:,FrameIndex);
    if ~mod((FrameIndex-1),hop_ratio)
       xi_l = max(param.alpha_DD*xi_unsmoothed_l+(1-param.alpha_DD)*max(zeta_l-1,param.Floor_xi),param.Floor_xi); % Update a priori SNR
    end
   
	G_l= STFT_GetGain(xi_l,zeta_l,param.Method);
    G(:,FrameIndex)  = G_l;
    xi(:,FrameIndex) = xi_l;
    xi_unsmoothed_l = zeta_l.*abs(G_l).^2; 
end
% floor gain:
G(abs(G)<param.G_min) = param.G_min;
% apply gain:
X_hat = Y.*G; 

%%%%%%%%%%%%%%%%%%% (7) Signal Synthesis: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X_hat = [X_hat;flipud(conj(X_hat(2:(end-1),:)))]; % restore full dft
x_hat_frames = ifft(X_hat); % inverse dft

x_hat = STFT_OLA(x_hat_frames,param);
x_hat = [x_hat(1:min(length(y),length(x_hat))); zeros(max(length(y)-length(x_hat),0),1)]; % ensure correct length

end
