function D_p_2 = PS_GetNoisePSD(y,Y,t_frames,fftlength,param)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimate Noise PSD via [1] and interpolate it to PSswDFT time-frequency
% points.
%
% INPUT:
% y ... noisy speech
% Y ... noisy spectrum
% t_frames ... timestamps of PS frames
% fftlength ... frame dependent framelength
% param ... struct containing parameters for the algorithm
%                            fs ... sampling frequency
%                           win ... window function handle
%
% OUTPUT:
% D_p_2 ... matrix that contains noise variance estimates at time-frequency points of the PS frame setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% author: Johannes Stahl (johannes.stahl@tugraz.at)
% last update: 28.02.2017
% matlab version: 2014b
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% References:
% [1] J. Stahl and P. Mowlaee "Detection and Estimation of Stochastic and
%     Deterministic Signal Components for Speech Enhancement", IEEE/ACM 
%     Trans. Audio, Speech, and LAnguage Processing, submitted 2017.
% [2] T. Gerkmann and R. C. Hendriks, “Unbiased MMSE-based noise power
%     estimation with low complexity and low tracking delay,” IEEE Trans.
%     Audio, Speech, and Language Process., vol. 20, no. 4, pp. 1383–1393,
%     2012.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bugs, comments, etc:
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     PS_GetNoisePSD.m estimates the noise PSD and interpolates the outcome to PSswDFT time-frequency points.
%     Copyright (C) 2017 Johannes Stahl
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

framelength = 32e-3;
hop = 0.004;
fs = param.fs;
N = framelength*fs;
hop = hop*param.fs;
window = param.win(N);
 % normalization
y_framed = buffer(y,N,N-hop,'nodelay'); % framing the signal
NrOfFrames2 = size(y_framed,2); 
y_framed = y_framed.*window(:,ones(1,NrOfFrames2)); % windowing the signal
t_frames2 = ((N/2+1):hop:(length(y)-N/2 + 1))/param.fs; % frame-center time-stamps
Y_un = fft(y_framed)/sum(window);
Y_un = Y_un(1:(N/2+1),:);
Y_p_un = Y_un.*conj(Y_un); % power spectrum of half sided stft

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) PUT YOUR NOISE PSD ESTIMATOR HERE
% 2) for [1] we used [2]     
% 3) should have the variable name D_p and dimensions: (fftlength/2+1) x #frames                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D_p_un = estnoiseg(((Y_un).*conj(Y_un)).',hop/fs).'; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NrOfFrames = length(t_frames);
D_p_2 = zeros(size(Y,1),NrOfFrames);
D_p_2(1:(N/2+1),:) = interp1(t_frames2,D_p_un(:,1:length(t_frames2)).',t_frames,'linear','extrap').';
for kk = 1:NrOfFrames
    D_p_2(1:round((fftlength(kk)-1)/2+1),kk) = interp1(fs*(0:(N/2))/N,D_p_2(1:(N/2+1),kk),fs*(0:round((fftlength(kk)-1)/2))/fftlength(kk),'spline','extrap');
end

end
